import Vue from 'vue'
import Vuex from 'vuex'
import Usuario from './modules/usuario/store'

Vue.use(Vuex)

const store = new Vuex.Store({
	modules: {
		Usuario,
	}
})

export default store