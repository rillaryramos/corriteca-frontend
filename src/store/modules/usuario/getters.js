const getters = {

    getUsuarioLogado (state){
        return state.usuario
    },

    isLoggedIn (state){
        return state.login
    }

}

export default getters