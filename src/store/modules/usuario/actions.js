import { HTTP } from '../../../shared/common/common'
import { router }  from '../../../routes/routes'
const actions = {

  login(context, usuario) {
    console.log(usuario)

    HTTP
      .post(`/login`, usuario)
      .then(response => {
        let token = response.data
        // let user = response.data.usuario
        console.log(response.data)
        localStorage.setItem("Authorization", token);
        let usuario = JSON.parse(response.config.data)
        localStorage.setItem("Usuario", usuario.login);
        // localStorage.setItem("usuario", JSON.stringify(user));
        context.commit('LOGIN_SUCCESS', true)
        router.push({ name: 'home'})
      })
      
  },

  logout(context) {
    localStorage.removeItem("Authorization");
    // localStorage.removeItem("usuario");
    context.commit('LOGOUT')
    router.push({ name: 'login'})
  }

}

export default actions