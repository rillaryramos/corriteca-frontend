const mutations = {

    LOGIN_SUCCESS (state, usuario) {
        state.login = true
        state.usuario = usuario
    },

    LOGOUT (state) {
        state.login = false
    }


}
export default mutations