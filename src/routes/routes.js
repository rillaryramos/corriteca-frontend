import Vue from 'vue'
import VueRouter from 'vue-router'
import LayoutSistema from '@/pages/sistema/LayoutSistema'
import Inicio from '@/pages/sistema/Inicio'
import Conferencias from '@/pages/sistema/conferencia/Conferencias'
import CadastroConferencia from '@/pages/sistema/conferencia/CadastroConferencia'
import EditarConferencia from '@/pages/sistema/conferencia/EditarConferencia'
import Corredores from '@/pages/sistema/corredor/Corredores'
import CadastroCorredor from '@/pages/sistema/corredor/CadastroCorredor'
import EditarCorredor from '@/pages/sistema/corredor/EditarCorredor'
import Duplas from '@/pages/sistema/dupla/Duplas'
import CadastroDupla from '@/pages/sistema/dupla/CadastroDupla'
import EditarDupla from '@/pages/sistema/dupla/EditarDupla'
import Apostas from '@/pages/sistema/aposta/Apostas'
import CadastroAposta from '@/pages/sistema/aposta/CadastroAposta'
import VisualizarAposta from '@/pages/sistema/aposta/VisualizarAposta'
import Vendedores from '@/pages/sistema/vendedor/Vendedores'
import CadastroVendedor from '@/pages/sistema/vendedor/CadastroVendedor'
import EditarVendedor from '@/pages/sistema/vendedor/EditarVendedor'
import CadastroResultado from '@/pages/sistema/resultado/CadastroResultado'
import VisualizarResultado from '@/pages/sistema/resultado/VisualizarResultado'
import PontuacaoApostadores from '@/pages/sistema/resultado/PontuacaoApostadores'
import Login from '@/pages/sistema/login/Login'
import Configuracoes from '@/pages/sistema/Configuracoes'

import axios from 'axios'

Vue.use(VueRouter)
var aposta = false;
var qtdAlteracaoAposta = '';
const teste = [
  axios.get('http://localhost:8080/api/configuracao').then(response => {
      aposta = response.data[0].apostasLiberadas
      qtdAlteracaoAposta = response.data[0].qtdAlteracao
      // console.log(this.aposta)
  })
];



const routes = [
    { path: '/login', name: 'login', component: Login },
    { path: '/', component: LayoutSistema, redirect: '/home', children: [
        { path: '/home', name: 'home', component: Inicio, meta: {requiresAuth: true} },
        { path: '/conferencias', name: 'conferencias', component: Conferencias, meta: {requiresAuth: true} },
        { path: '/nova_conferencia', name: 'nova_conferencia', component: CadastroConferencia, meta: {requiresAuth: true} },
        { path: '/conferencia/:id', name: 'editar_conferencia', component: EditarConferencia, props: true, meta: {requiresAuth: true} },
        { path: '/corredores', name: 'corredores', component: Corredores, meta: {requiresAuth: true} },
        { path: '/novo_corredor', name: 'novo_corredor', component: CadastroCorredor, meta: {requiresAuth: true} },
        { path: '/corredor/:id', name: 'editar_corredor', component: EditarCorredor, props: true, meta: {requiresAuth: true} },
        { path: '/duplas', name: 'duplas', component: Duplas, meta: {requiresAuth: true} },
        { path: '/nova_dupla', name: 'nova_dupla', component: CadastroDupla, meta: {requiresAuth: true} },
        { path: '/dupla/:id', name: 'editar_dupla', component: EditarDupla, props: true, meta: {requiresAuth: true} },
        { path: '/apostas', name: 'apostas', component: Apostas, meta: {requiresAuth: true} },
        { path: '/nova_aposta/:permitir', name: 'nova_aposta', component: CadastroAposta, meta: {requiresAuth: true} },
        { path: '/aposta/:id', name: 'visualizar_aposta', component: VisualizarAposta, props: true, meta: {requiresAuth: true} },
        { path: '/vendedores', name: 'vendedores', component: Vendedores, meta: {requiresAuth: true} },
        { path: '/novo_vendedor', name: 'novo_vendedor', component: CadastroVendedor, meta: {requiresAuth: true} },
        { path: '/vendedor/:id', name: 'editar_vendedor', component: EditarVendedor, props: true, meta: {requiresAuth: true} },
        { path: '/resultado', name: 'resultado', component: CadastroResultado, meta: {requiresAuth: true} },
        { path: '/visualizar', name: 'visualizar', component: VisualizarResultado, meta: {requiresAuth: true} },
        { path: '/configuracoes', name: 'configuracoes', component: Configuracoes, meta: {requiresAuth: true} },
        { path: '/pontuacao', name: 'pontuacao', component: PontuacaoApostadores, meta: {requiresAuth: true} }
      ]
    } 
]

export const router = new VueRouter({
  	mode: 'history',
    routes,
    // teste
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
      if (localStorage.getItem('Authorization') == null) {
        next({ path : '/login', component: Login })
      }
    }
    next();
});


router.beforeEach((to, from, next) => {
    if(aposta == false){
        if(to.name == "nova_aposta"){
            next({ path : '/apostas', component: Duplas })
        } else {
            next()
        }
    }else {
        next()
    }
    console.log(aposta)
});

router.beforeEach((to, from, next) => {
    if(qtdAlteracaoAposta > 0){
        if(to.name == "novo_corredor" || to.name == "editar_corredor"){
            next({ path : '/corredores', component: Corredores })
        }else if(to.name == "nova_dupla" || to.name == "editar_dupla"){
            next({ path : '/duplas', component: Duplas })
        }else {
            next()
        }
    }
    else {
        next()
    }
});
