import axios from 'axios';

const token = localStorage.getItem('Authorization')


export const HTTP = axios.create({
  baseURL: `http://localhost:8080`,
  headers: {
    'Authorization': `${token}`,
    'crossDomain': true,
    'withCredentials': true,
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': ['*'],
    'Content-Type': 'application/json',
  }
})
