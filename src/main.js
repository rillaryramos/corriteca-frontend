import Vue from 'vue'
import axios from 'axios'
import App from './App'
import { router } from './routes/routes'
import store from './store/index'
import iView from 'iview';
import 'iview/dist/styles/iview.css'
import 'materialize-css/dist/css/materialize.css'
// import Vuetify from 'vuetify'
import VueTheMask from 'vue-the-mask'
import AsyncComputed from 'vue-async-computed'
// import VueResource from 'vue-resource'

Vue.config.productionTip = false
Vue.use(AsyncComputed);
Vue.use(iView)
// Vue.use(Vuetify)
Vue.use(VueTheMask)

const token = localStorage.getItem('Authorization')
if (token) {
  axios.defaults.headers.common['Authorization'] = `${token}`
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})


