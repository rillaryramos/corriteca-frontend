import axios from "axios";
import {
    HTTP
  } from '../../../shared/common/common'
export const mxDupla = {

    data() {
        return {
            formDupla: {
                id: '',
                corredor1: {
                    id: '',
                    pessoa: {
                        nome: ''
                    }
                },
                corredor2: {
                    id: '',
                    pessoa: {
                        nome: ''
                    }
                }
            },

            pesquisaCorredor: '',
            nomeCorredor1: '',
            nomeCorredor2: '',

            corredores: [],
        }
    },

    methods: {
        listarCorredores() {
            HTTP.get("/api/corredores").then(response => {
                this.corredores = response.data;
                
                let promises = this.corredores.map(corredor => 
                    HTTP.get(`/api/corredores/verificarCorredor/${corredor.id}`)
                );

                return promises.reduce((promiseChain, corredorAtual) => {
                    return promiseChain.then(chainResults => {
                        return corredorAtual.then(currentResult => [...chainResults, currentResult.data])
                    })
                }, Promise.resolve([]));
            }).then(statusDuplas => {
                this.corredores = statusDuplas.map((status, i) => {
                    this.corredores[i].emDupla = status;
                    return this.corredores[i];
                });
            })
        },

        iniciarPesquisaCorredor() {
            this.$Loading.start();
            this.pesquisarCorredor()
        },

        pesquisarCorredor: _.debounce(function () {
            if(this.pesquisaCorredor != ''){
                HTTP.get(`/api/corredores/1/${this.pesquisaCorredor}`).then(response => {
                    this.corredores = response.data
                    let promises = this.corredores.map(corredor => 
                        HTTP.get(`/api/corredores/verificarCorredor/${corredor.id}`)
                    );

                    return promises.reduce((promiseChain, corredorAtual) => {
                        return promiseChain.then(chainResults => {
                            return corredorAtual.then(currentResult => [...chainResults, currentResult.data])
                        })
                    }, Promise.resolve([]));
                }).then(statusDuplas => {
                    this.corredores = statusDuplas.map((status, i) => {
                        this.corredores[i].emDupla = status;
                        return this.corredores[i];
                    });
                }) 
            } else{
                this.listarCorredores();
            }      
            this.$Loading.finish();
        }, 1500),

        warning (nodesc) {
            this.$Notice.warning({
                title: 'Notificação',
                desc: nodesc ? '' : 'Este corredor já está participando de uma dupla. '
            });
        },

        warningDuplaAtual (nodesc) {
            this.$Notice.warning({
                title: 'Notificação',
                desc: nodesc ? '' : 'Este corredor já foi adicionado a esta dupla. '
            });
        },

        warningSexoDiferente (nodesc) {
            this.$Notice.warning({
                title: 'Notificação',
                desc: nodesc ? '' : 'Corredores de uma dupla devem ser do mesmo sexo. '
            });
        },

        buscarNomeCorredor(idCorredor) {
            var i
            var posicao
            for(i=0; i < this.corredores.length; i++){
                if(this.corredores[i].id == idCorredor){
                    posicao = i 
                }
            }
            return this.corredores[posicao].pessoa.nome
        },
        
        buscarSexoCorredor(idCorredor) {
            var i
            var posicao
            for(i=0; i < this.corredores.length; i++){
                if(this.corredores[i].id == idCorredor){
                    posicao = i 
                }
            }
            return this.corredores[posicao].sexo.descricao
        },

        addCorredor(idCorredor) {
            if(this.formDupla.corredor1.id === '') {
                if (this.formDupla.corredor2.id != '' && (this.buscarSexoCorredor(idCorredor) != this.buscarSexoCorredor(this.formDupla.corredor2.id))) {
                    this.warningSexoDiferente(false)
                } if (this.formDupla.corredor2.id != '' && idCorredor == this.formDupla.corredor2.id) {
                    this.warningDuplaAtual(false)
                } else{
                    this.formDupla.corredor1.id = idCorredor
                    this.nomeCorredor1 = this.buscarNomeCorredor(idCorredor)
                }
            }else if(this.formDupla.corredor1.id != '' && this.formDupla.corredor2.id == ''){
                if(idCorredor == this.formDupla.corredor1.id) {
                    this.warningDuplaAtual(false)
                }else if(this.buscarSexoCorredor(idCorredor) != this.buscarSexoCorredor(this.formDupla.corredor1.id)){
                    this.warningSexoDiferente(false)
                }else {
                    this.formDupla.corredor2.id = idCorredor
                    this.nomeCorredor2 = this.buscarNomeCorredor(idCorredor)
                }
            }
        },

        ResetCorredor1() {
            this.formDupla.corredor1.id = ''
            this.nomeCorredor1 = ''
        },

        ResetCorredor2() {
            this.formDupla.corredor2.id = ''
            this.nomeCorredor2 = ''
        },

        handleReset () {
            this.ResetCorredor1();
            this.ResetCorredor2();
        }
    }
}